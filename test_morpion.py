# Fonctions de test du TP7
#  ATTENTION VOUS DEVEZ COMPLETER LES TESTS
#  pour appeler vos fonctions ecrivez: morpion.ma_fonction


import morpion as morpion


def test_init_grille():
    ...




def csv_to_liste(fichier):
    fic=open(fichier,'r')
    liste = []
    for ligne in fic:
        ligne_decoupee = ligne[:-1].split(";")
        liste.append(ligne_decoupee)
    return liste


def verification_des_lignes(joueur, grille):
    gagne = False
    if grille[0][0] == joueur and grille[0][0] == grille[0][1] and  grille[0][1] == grille[0][2]:
        gagne = True
    elif grille[1][0] == joueur  and grille[1][0] == grille[1][1] and  grille[1][1] == grille[1][2]:
        gagne = True
    elif grille[2][0] == joueur  and grille[2][0] == grille[2][1] and  grille[2][1] == grille[2][2]:
        gagne = True
    return gagne


def verification_des_colonnes(joueur, grille):
    gagne = False
    if grille[0][0] == joueur and grille[0][0] == grille[1][0] and  grille[1][0] == grille[2][0]:
        gagne = True
    if grille[0][1] == joueur and grille[0][1] == grille[1][1] and  grille[1][1] == grille[2][1]:
        gagne = True
    if grille[0][2] == joueur and grille[0][2] == grille[1][2] and  grille[1][2] == grille[2][2]:
        gagne = True
    return gagne


def verification_diagonale1(joueur, grille):
    gagne = False
    if grille[0][0] == joueur and grille[0][0] == grille[1][1] and  grille[1][1] == grille[2][2]:
        gagne = True
    if grille[2][0] == joueur and grille[2][0] == grille[1][1] and  grille[1][1] == grille[0][2]:
        gagne = True
    return gagne


def test_gagne(joueur, grille):
    ...


def joue(joueur, grille, ligne, colonne):
    """
    Le joueur joue à la position ligne/colonne.
    La fonction met le jeu à jour et envoie True si tout s'est bien passé
    False sinon
    """
    grille[ligne][colonne] = joueur


def jeu_est_fini(grille):
    for ligne in grille:
        for case in ligne:
            if case is None:
                return False
    return T


def coup_valide(ligne, colonne, jeu):
    """
    Le joueur joue à la position ligne/colonne.
    La fonction met le jeu à jour et envoie True si tout s'est bien passé
    False sinon
    """
    return jeu[ligne][colonne] is None

def choix(message):
    return int(input(message + " "))



def lance_jeu():
    grille = init_grille()
    joueur = init_joueur()
    fini = False
    while not fini:
        utilitaires_tableaux.affiche(grille)
        print("Au joueur", joueur, "de jouer")
        ligne = choix("Quelle ligne ? ")
        colonne = choix("Quelle colonne ? ")
        if coup_valide(ligne, colonne, grille):
            joue(joueur, grille, ligne, colonne)
            if gagne(joueur, grille):
                fini = True
                print("Le gagnant est ", joueur)
            elif jeu_est_fini(grille):
                fini = True
                print("La parie est terminée et il n'y a pas de gagnant")
            else:
                joueur = change(joueur)
        else:
            print("Attention !! Ce coup n'est pas valide")


def affiche_menu():
    """ Affiche un menu qui permet de joueur au jeu du morpion """
    fin = False
    while not fin:
        reponse = input("=====================\nJeu du morpion\nQuel est votre choix ?\n(J)ouer ou (Q)uitter ? ")
        if reponse == 'J':
            lance_jeu()
        elif reponse == 'Q':
            fin = True


affiche_menu()
